function hablar (algo){
    console.log (algo)
}
function hablar_bajito(algo){
    console.log (algo.toLowerCase());
}
function gritar (algo){
    console.log(algo.toUpperCase());
}
//exportamos las funciones, las vamos a llevar a archivo imports.js
exports.hablar = hablar;
exports.hablar_bajito = hablar_bajito;
exports.gritar = gritar;

