//leer sistema de archivo de la compu, usando la palabra fs que hace referencia a file sistem
const fs = require('fs');
//el ./ hace referencia a donde estamos parados
const folderPath = './mi-primer-proyecto/src/node/';
//usamos el metodo readdir, que lee directorio
// fs.readdir(folderPath, (err,files) => {
//     if (err){
//         console.log (err)

//     }else{
//         console.log (files)
//     }
// });
//de esta forma te muestra todos los archivos que tengas en tu proyecto en forma de lista, en forma de array

//para q los muestre en forma de lista
fs.readdir(folderPath, (err,files) => {
    if (err){
        console.log (err)

    }else{
        files.forEach(files =>{
            console.log (files)
        });
        
    }
});